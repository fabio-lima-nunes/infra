# README
# TERRAFORM
Para provisionar a infraestrutura na nuvem aws apenas executar os comandos abaixo:

## Criar um arquivo main.tf na raiz do projeto e subistituir os valores, use como exemplo o main-example.txt
    touch main.tf
## Inicializar o terraform
    terraform init
## Validar os arquivos de configurações:
    terraform validate
## Planejar e Executar o terraform de acordo um determinado perfil da aws configurado no seu computador:
    terraform plan --var profile=meuprofile
    terraform apply --var profile=meuprofile --auto-approve
# ANSIBLE
## Para provisionar as configurações da máquina, no caso utilizei em uma EC2, execute os seguintes comandos:
    ansible-playbook -i hosts docker.yaml -v -u user.name
    ansible-playbook -i hosts codedeploy-agent.yaml -v -u user.name


