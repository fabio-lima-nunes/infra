variable "tag_cost" {}
variable "tag_project" {}
variable "tag_parent" {}

variable "vpc_default" {}

variable "name" {}
variable "ami" {}
variable "instance_type" {}
variable "key_name" {}
variable "subnet_id" {}
